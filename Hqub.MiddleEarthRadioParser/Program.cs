﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Hqub.MiddleEarthRadioParser
{
    class Program
    {
        private static List<string> _songsCollection;

        static void Main(string[] args)
        {
            var web = new HtmlAgilityPack.HtmlWeb
                {
                    AutoDetectEncoding = false,
                    OverrideEncoding = Encoding.GetEncoding(Properties.Settings.Default.Encoding)
                };


            if (System.IO.File.Exists(Properties.Settings.Default.Path))
            {
                _songsCollection = new List<string>(System.IO.File.ReadAllLines(Properties.Settings.Default.Path));
            }
            else
            {
                _songsCollection = new List<string>();
            }


            do
            {
                Console.Clear();
                ColorWrite(string.Format("({0}) Обработка списка начата.", DateTime.Now), ConsoleColor.Yellow);

                var document = web.Load(Properties.Settings.Default.SourceUrl, "GET");

                var tables = document.DocumentNode.SelectNodes("//table");

                if (tables.Count < 3)
                {
                    ColorWrite("Неверный формат верстки.", ConsoleColor.Red);
                    Console.ReadKey();
                    return;
                }

                var tableWithSongs = tables[2];

                var rows = tableWithSongs.SelectNodes("tr");

                for (var i = 1; i < rows.Count; i++)
                {
                    var currentNode = rows[i];

                    var columns = currentNode.SelectNodes("td");

                    if (columns.Count < 2)
                        continue;

                    var songName = columns[1].InnerText;

                    if (!_songsCollection.Contains(songName))
                    {
                        ColorWrite(string.Format("\t{0}", songName), ConsoleColor.Green);
                        _songsCollection.Add(songName);
                    }
                    else
                    {
                        ColorWrite(string.Format("\t{0}", songName), ConsoleColor.Gray);
                    }
                }

                Save(_songsCollection);
                System.Threading.Thread.Sleep(Properties.Settings.Default.Interval);

                Console.WriteLine();
               

            } while (true);
        }

        static void Save(IEnumerable<string> songCollection)
        {
            System.IO.File.WriteAllLines(Properties.Settings.Default.Path, songCollection, Encoding.GetEncoding(Properties.Settings.Default.Encoding));
        }


        static void ColorWrite(string text, ConsoleColor consoleColor)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(text);
            Console.ResetColor();
        }
    }
}
